<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Percabangan</title>
    <style>
    .container{
    display: flex;
    align-content: center;
    justify-content: center;
    margin-top: 150px;
  }
  .box{
    border-radius: 10px;
    border: 1px;
    padding: 10px;
    text-align: start;
    background-color: #804040;
  }
  .box p{
    font-weight: bold;
  }
    </style>
</head>
<body>
    <div class="container">
        <div class="box">
        <h1>Percabangan Switch Case Dengan PHP</h1>
    <?php
    echo "Your Favorite Food: </br>";
    echo "1. Nasgor </br>";
    echo "2. Nasdang </br>";
    echo "3. Mie </br>";
    $favfood = "nasgor";

    switch ($favfood) {
        case "nasgor":
            echo "Your favorite food is nasgor!";
            break;
        case "nasdang":
            echo "Your favorite food is nasdang!";
            break;
        case "mie":
            echo "Your favorite food is mie!";
            break;
        default:
            echo "Your favorite food is neither nasgor, nasdang, nor mie!";
        }
    ?>
        </div>
    </div>
</body>
</html>