<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perulangan For</title>
    <style>
    .container{
    display: flex;
    align-content: center;
    justify-content: center;
    margin-top: 150px;
  }
  .box{
    border-radius: 10px;
    border: 1px;
    padding: 10px;
    text-align: start;
    background-color: #804040;
  }
  .box p{
    font-weight: bold;
  }
    </style>
</head>
<body>
    <div class="container">
        <div class="box">
        <h1>Perulangan For Dengan PHP</h1>
    <p>Angka Fibonaci setelah angka 3 & 7</p>
    <?php
    echo"Angka Fibonaci : 3 7 ";
    $a=3;
    $b=7;
    for($i =1; $i<=8; $i++){
        $tmp = $a + $b;
        echo"$tmp";
        echo" ";
        $a=$b;
        $b= $tmp;
    }
    ?>
        </div>
    </div>
</body>
</html>