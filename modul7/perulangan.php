<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perulangan While</title>
    <style>
    .container{
    display: flex;
    align-content: center;
    justify-content: center;
    margin-top: 150px;
  }
  .box{
    border-radius: 10px;
    border: 1px;
    padding: 10px;
    text-align: start;
    background-color: #804040;
  }
  .box p{
    font-weight: bold;
  }
    </style>
</head>
<body>
    <div class="container">
        <div class="box">
        <h1>Perulangan While Dengan PHP</h1>
    <?php
    $c = 10;
    while($c>0){
        echo"Anak Ayam Turun $c";
        $kurang = $c-1;
        echo " Mati Satu Tinggal $kurang <br/>";
        $c--;
    }
    ?>
        </div>
    </div>
</body>
</html>