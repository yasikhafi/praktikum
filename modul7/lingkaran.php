<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lingkaran</title>
    <style>
    .container{
    display: flex;
    align-content: center;
    justify-content: center;
    margin-top: 150px;
  }
  .box{
    border-radius: 10px;
    border: 1px;
    padding: 10px;
    text-align: start;
    background-color: #804040;
  }
  .box p{
    font-weight: bold;
  }
    </style>
</head>
<body>
    <div class="container">
        <div class="box">
        <h1>Hitung Luas Lingkaran Dengan PHP</h1>
    <?php
            $jari   =14;
            $phie    =3.14;
            
            // menghitung luas lingkaran
            $luas_lingkaran = $phie*($jari*$jari);
            
            echo "Hasil hitung luas lingkaran adalah sebagai berikut:<br />";
            echo "Diketahui;<br />";
            echo "Jari-jari lingkaran = $jari<br />";
            echo "Phie = $phie<br />";
            echo "Maka luas lingkaran sama dengan [ $phie x $jari x $jari ] = $luas_lingkaran";
    ?>
        </div>
    </div>
</body>
</html>